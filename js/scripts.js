

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 30, window.innerWidth/window.innerHeight, 0.1, 1500 );
var mesh;

var renderer = new THREE.WebGLRenderer({alpha: true});
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var geometry = new THREE.CylinderGeometry(5, 5, 25);
THREE.ImageUtils.crossOrigin = true;
var textureLoader = new THREE.TextureLoader();
textureLoader.crossOrigin = true;
textureLoader.load("https://cdn.shopify.com/s/files/1/2271/0427/products/flow-pink-3_1500x1500.jpg?v=1541174016", function(texture) {
  texture.wrapS = texture.wrapT =   THREE.RepeatWrapping;
    texture.repeat.set( 2, 2 );
    var material = new THREE.MeshLambertMaterial( {map: texture} );
  mesh = new THREE.Mesh( geometry, material );
  scene.add( mesh );


  
  render();
});


camera.position.z = 100;


var light = new THREE.DirectionalLight( 0xffffff, 1 );
light.position.set( 0, 1, 0 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
light.position.set( 0, -1, 0 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 1 );
light.position.set( 1, 0, 0 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
light.position.set( 0, 0, 1 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 1 );
light.position.set( 0, 0, -1 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
light.position.set( -1, 0, 0 );
scene.add( light );



var render = function () {
  requestAnimationFrame( render );
  mesh.rotation.z += 0.01;
  mesh.rotation.x -= 0.05;
  mesh.rotation.y += 0.02;
  renderer.render(scene, camera);
};


